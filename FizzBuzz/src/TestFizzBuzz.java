import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestFizzBuzz {

	@Test
	void testNumeroNormal() {
		FizzBuzz fizz = new FizzBuzz();	
		assertEquals("7",fizz.getFizzBuzz(7));
	}
	
	
	@Test
	void testFizz() {	
		FizzBuzz fizz = new FizzBuzz();
		assertEquals("Fizz",fizz.getFizzBuzz(3));
	}
	

	@Test
	void testBuzz() {		
		FizzBuzz fizz = new FizzBuzz();
		assertEquals("Buzz",fizz.getFizzBuzz(5));
	}
	
	@Test
	void testFizzBuzz() {
		FizzBuzz fizz = new FizzBuzz();
		assertEquals("FizzBuzz",fizz.getFizzBuzz(60));
	}

}



